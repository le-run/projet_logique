(******************************************************************************)
(*                                                                            *)
(*                      INVERSION DE MD* VIA SAT SOLVEUR                      *)
(*                                                                            *)
(*                      Projet Logique 2016 - Partie SAT                      *)
(*   Auteur, license, etc.                                                    *)
(******************************************************************************)

(* Input: only one chunk of 512 bits (16 words of 32 bits), NO padding
   Output: digest of 128 bits (WITHOUT padding)
By default:
- 4 rounds for the only chunk
- each round is made of 16 steps
- each step consume a word of the input (not linear !)
- can be seen as 64 steps such that non-linear functions (i.e., F,G,H,I), and
  parts of inputs that are consumed depend on the current step
 *)

open Param
open Data
open Printf

let leftrotate value distance =
  (Int32.logor (Int32.shift_left value distance) (Int32.shift_right_logical value (Int32.to_int (Int32.sub (Int32.of_int 32) (Int32.of_int distance)))))
       
(*** Main function ***)	  
let compute input =
  let a = ref (arr_to_int32 a0) in
  let b = ref (arr_to_int32 b0) in
  let c = ref (arr_to_int32 c0) in
  let d = ref (arr_to_int32 d0) in
  let m j =
    arr_to_int32 (Array.sub input (j*32) 32)
  in
  let f = ref Int32.zero in
  let g = ref 0 in
  for round = 1 to !rounds do
      begin
	for step = 1 to !steps do
	    begin
	      let i = ((round-1) * !steps) + (step-1) in
	      begin
		match round with
		| 1 -> 
		  begin
		    f := (Int32.logor (Int32.logand !b !c) (Int32.logand (Int32.lognot !b) !d));
		    g := i;
		    ()
		  end
		| 2 ->
		  begin
		    f := (Int32.logor (Int32.logand !d !b) (Int32.logand (Int32.lognot !d) !c));
		    g := (5 * i + 1) mod 16;
		    ()
		  end
		| 3 ->
		  begin
		    f := (Int32.logxor (Int32.logxor !b !c) !d);
		    g := (3 * i + 5) mod 16;
		    ()
		  end
		| 4 ->
		  begin
		    f := (Int32.logxor !c (Int32.logor !b (Int32.lognot !d)));
		    g := (7 * i) mod 16;
		    ()
		  end
		| _ -> raise (Failure "Wrong round amount")
	      end;
	      let dtemp = !d in
	      d := !c;
	      c := !b;
	      b := (Int32.add !b
		      (leftrotate
			 (Int32.add !a (Int32.add !f (Int32.add (arr_to_int32 vectK.(i)) (m !g))))
			 vectS.(i)));
	      a := dtemp;
	      ()
	    end
	done
      end
  done;
  Printf.printf "\n";
  let a0 = int32_to_arr (Int32.add (arr_to_int32 a0) !a) in
  let b0 = int32_to_arr (Int32.add (arr_to_int32 b0) !b) in
  let c0 = int32_to_arr (Int32.add (arr_to_int32 c0) !c) in
  let d0 = int32_to_arr (Int32.add (arr_to_int32 d0) !d) in
  Array.append (Array.append (Array.append a0 b0) c0) d0
