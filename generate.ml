(******************************************************************************)
(*                                                                            *)
(*                      INVERSION DE MD* VIA SAT SOLVEUR                      *)
(*                                                                            *)
(*                      Projet Logique 2016 - Partie SAT                      *)
(*   Auteur, license, etc.                                                    *)
(******************************************************************************)
		
open Param
open Printf
open Formula
open Data

module LiteralGen : sig
  val getCounter : unit -> int
  val newLit : unit -> int
  val newInt : unit -> int
end = struct
  let counter = ref 513
  let getCounter () = !counter
  let newLit () = let c = !counter in counter := !counter + 1; c (* Generates a new unused literal id *)
  let newInt () = let c = !counter in counter := !counter + 32; c (* Generates 32 new consecutive literal ids representing a 32 bits integer *)
end
open LiteralGen

module CNF : sig
  val add : formula -> unit
  val get : unit -> formula list
end = struct
  let l = ref []
  let add clause = l := clause::!l
  let get () = !l
end

let addTwo startlita startlitb startlits =
  let carry = ref (Pos (newLit())) in
  CNF.add (Not (Lit !carry));
  for i = 0 to 31 do
      begin
	let newCarry = Pos (newLit()) in
	let xor1 = Xor (Lit (Pos (startlita + i)),Lit (Pos (startlitb + i))) in
	let sumClause = Equiv (Xor (xor1,Lit !carry),Lit (Pos (startlits + i))) in
	let carryClause = Equiv (
	  Or (
	    And ( xor1, Lit !carry ),
	    And (Lit (Pos (startlita + i)),Lit (Pos (startlitb + i)))),
	  Lit newCarry)
	in
	carry := newCarry;
        CNF.add sumClause;
	CNF.add carryClause
      end
  done

let addFour (a:int) (b:int) (c:int) (d:int) (s:int) =
  let res1 = (newInt()) in
  let res2 = (newInt()) in
  addTwo a b res1;
  addTwo c d res2;
  addTwo res1 res2 s

let f_func b c d f =
  for i=0 to 31 do
      begin
	CNF.add (Or (Or (Lit (Pos (b+i)),Lit (Pos (d+i))),Lit (Neg (f+i))));
	CNF.add (Or (Or (Lit (Pos (b+i)),Lit (Neg (d+i))),Lit (Pos (f+i))));
	CNF.add (Or (Or (Lit (Neg (b+i)),Lit (Pos (c+i))),Lit (Neg (f+i))));
	CNF.add (Or (Or (Lit (Neg (b+i)),Lit (Neg (c+i))),Lit (Pos (f+i))));
      end
  done

let g_func b c d f =
  for i=0 to 31 do
    begin
      (*
	CNF.add (Or (Or (Lit (Pos (b+i)),Lit (Pos (d+i))), Lit (Pos (f+i))));
	CNF.add (Or (Or (Lit (Neg (b+i)),Lit (Pos (d+i))), Lit (Neg (f+i))));
	CNF.add (Or (Or (Lit (Pos (c+i)),Lit (Neg (d+i))), Lit (Pos (f+i))));
	CNF.add (Or (Or (Lit (Neg (c+i)),Lit (Neg (d+i))), Lit (Neg (f+i))));
      *)
      let clause = Equiv(
	Lit (Pos (f+i)),
	Or (
	  And (Lit (Pos (d+i)), Lit (Pos (b+i))),
	  And (Lit (Neg (d+i)), Lit (Pos (c+i)))))
      in
      CNF.add clause;
    end
  done

let h_func b c d f =
  let rec loop acc = if acc = 0 then () else
      begin
	let i = 32 - acc in
	let clause =
	  Equiv(
	    Xor (Xor (Lit (Pos (b+i)),Lit (Pos (c+i))),Lit (Pos (d+i))),
	    Lit (Pos (f+i)))
	in
	CNF.add clause;
	loop (acc-1)
      end
  in
  loop 32

let i_func b c d f =
  let rec loop acc = if acc = 0 then () else
      begin
	let i = 32 - acc in
	let clause =
	  Equiv(
	    Xor(
	      Lit (Pos (c+i)),
	      Or (Lit (Pos (b+i)),Lit (Neg (d+i)))),
	    Lit (Pos (f+i)))
	in
	CNF.add clause;
	loop (acc-1)
      end
  in
  loop 32

let left_rotate value distance result =
  let modulus a b =
    if a < 0 then (a mod b) + b else a mod b
  in
  for i=0 to 31 do
      begin
	let clause = Equiv(
	  Lit (Pos (value+(modulus (i-distance) 32))),
	  Lit (Pos (result+i)))
	in
	CNF.add clause;
      end
  done

let equals a b =
  for i=0 to 31 do
      begin
	let clause = Equiv (Lit (Pos (a+i)),Lit (Pos (b+i))) in
	CNF.add clause;
      end
  done

let setTo startlit (value:Int32.t) =
  let array = int32_to_arr value in
  for i=0 to 31 do
      begin
	CNF.add (Lit (if array.(i) then Pos (startlit + i) else Neg (startlit + i)));
      end
  done

(*** Main function ***)
let genCNF digest =
  log "Generating CNF...";
  begin
    match !partialKnownInput with
    | None -> ()
    | Some filename ->
      begin
	let input_file = open_in filename in
	let str = input_line input_file in
	let input = Data.parseInput str in
	let set_bool b prop =
	  CNF.add (if b then Lit (Pos prop) else Lit (Neg prop))
	in
	for i=0 to (!partialSize-1) do
	  set_bool input.(i) (i+1);
	done
      end
  end;
  let to_prop value = setTo (newInt()) value in
  (* reserve vectK *)
  let kof =
    let k_base = (getCounter()) in
    (fun i -> k_base + 32 * i)
  in
  Array.iter (fun value -> to_prop (arr_to_int32 value)) vectK;
  (* M *)
  let mof i =
    i * 32 + 1
  in
  (* reserve Initializers *)
  let a0_base = (newInt()) in
  setTo a0_base (arr_to_int32 a0);
  let b0_base = (newInt()) in
  setTo b0_base (arr_to_int32 b0);
  let c0_base = (newInt()) in
  setTo c0_base (arr_to_int32 c0);
  let d0_base = (newInt()) in
  setTo d0_base (arr_to_int32 d0);
  (* Initialize *)
  let a = ref (newInt()) in
  let b = ref (newInt()) in
  let c = ref (newInt()) in
  let d = ref (newInt()) in
  equals !a a0_base;
  equals !b b0_base;
  equals !c c0_base;
  equals !d d0_base;
  for round = 1 to !rounds do
      begin
	for step = 1 to !steps do
	    begin
	      let i = ((round-1) * !steps) + (step-1) in
	      let f = (newInt()) in
	      let g =
		match round with
		| 1 ->
		  begin
		    f_func !b !c !d f;
		    i
		  end
		| 2 ->
		  begin
		    g_func !b !c !d f;
		    (5 * i + 1) mod 16
		  end
		| 3 ->
		  begin
		    h_func !b !c !d f;
		    (3 * i + 5) mod 16
		  end
		| 4 ->
		  begin
		    i_func !b !c !d f;
		    (7 * i) mod 16
		  end
		| _ -> raise (Failure "Wrong round amout")
	      in
	      let dtmp = !d in
	      d := (newInt());
	      equals !d !c;
	      c := (newInt());
	      equals !c !b;
	      let sum = (newInt()) in
	      addFour
		!a
		f
		(kof i)
		(mof g)
		sum;
	      let rotated = (newInt()) in
	      left_rotate sum (vectS.(i)) rotated;
	      let lastb = !b in
	      b := (newInt());
	      addTwo lastb rotated !b;
	      a := (newInt());
	      equals !a dtmp;
	    end
	done
      end
  done;
  let afinal = (newInt()) in
  let bfinal = (newInt()) in
  let cfinal = (newInt()) in
  let dfinal = (newInt()) in
  addTwo !a a0_base afinal;
  addTwo !b b0_base bfinal;
  addTwo !c c0_base cfinal;
  addTwo !d d0_base dfinal;
  setTo afinal (arr_to_int32 (Array.sub digest 0  32));
  setTo bfinal (arr_to_int32 (Array.sub digest 32 32));
  setTo cfinal (arr_to_int32 (Array.sub digest 64 32));
  setTo dfinal (arr_to_int32 (Array.sub digest 96 32));
  formulaeToCnf (CNF.get())
