
./hackMD -v -r 1 -s 16 -c exemples/honest-input.hex
./hackMD -v -r 1 -s 16 -p 224 exemples/malicious-partial-input.hex exemples/honest-input__digest.out
echo -e "\n  # The text:"
echo -e -n "<<"
cat exemples/honest-input.hex | xxd -r -p
echo -e -n "\n>>"
echo -e "\n  # has the same MD5-digest as the following malicious text:"
echo -e -n "<<"
cat exemples/honest-input__digest__inputFound.out | xxd -r -p
echo -e -n "\n>>"
