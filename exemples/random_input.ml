(* Takes no argument and returns a random input in random_input.hex *)
open Printf;;
  
Random.init (int_of_float(Unix.gettimeofday ()));;
  
let res = ref "";;
let random_char () =
  let r= Random.int 16 in
  match r with
   |0 -> "0"
   |1 -> "1"
   |2 -> "2"
   |3 -> "3"
   |4 -> "4"
   |5 -> "5"
   |6 -> "6"
   |7 -> "7"
   |8 -> "8"
   |9 -> "9"
   |10 -> "a"
   |11 -> "b"
   |12 -> "c"
   |13 -> "d"
   |14 -> "e"
   |15 -> "f";;
  
for i = 1 to 16 do
  let current_word = ref "0x" in
  for i = 1 to 8 do
    current_word := !current_word ^ (random_char ())
  done;
  res := !res ^ !current_word;
  if i < 16
  then res := !res ^ " "
done;

let oc = open_out "random_input.hex" in
fprintf oc "%s\n" !res;
close_out oc;;
