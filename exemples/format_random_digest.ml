open Printf;; (* INDEX OUT OF BOUNDS ?! *)
  
let ic = open_in "random_digest_temp.hex";;

let line = ref " ";;

while !line.[0] <> '=' do
  (* Let us select the line we want ; it starts with ==> *)  
  line := input_line ic
done;;

while !line.[0] <> 'O' do
  (* Now we need to let the string start at the first "Ox..." *)
  line := String.sub !line 1 (String.length(!line)-1)
done;;
 
(* Now we need to ommit the last two characters, " ]" *)
line := String.sub !line 0 (String.length(!line)-2);;

(* Now to turn the Os into 0s *)
let f x=
  match x with
  |'O' -> '0'
  |_ -> x;;

line := String.map f !line;;
  
  
let oc = open_out "random_digest.hex";;
fprintf oc "%s" !line;
close_in ic;
close_out oc;;
