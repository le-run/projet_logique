open List;;

let average (l: float list) =
  let res = ref 0.
  and ll = ref l
  and n = float_of_int(length l) in
  while !ll <> [] do
    res := (!res) +. ((hd !ll)/.n);
    ll := tl !ll
  done;
  !res;;

let std_deviation (l: float list) =
  average (map (function x -> abs_float (x -. (average l))) l);;
