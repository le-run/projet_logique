open Printf;;

let n_val = 16 + 12;;
let n_exp = 1;;
  
let values = Array.make (n_val) [];;
  (* This will hold the values *)
let stats = Array.make (n_val) (0.,0.);;
(* And this will hold [|average; standard deviation|] *)

  

let read_files () =  
  (* Let us read the files *)
  for i = 1 to n_val do
    let file_name = "../results/simulation"^(string_of_int i)^".log" in
    let ic = open_in file_name in
    (* Time to read the file line after line *)
    for j = 1 to (n_exp) do
      let line = input_line ic in
      values.(i-1) <- (float_of_string line) :: (values.(i-1))
    done;
  done;;


let compute_stats () =
  for i = 0 to n_val-1 do
    stats.(i) <- (Tools.average values.(i),Tools.std_deviation values.(i))
  done;;

let write_stats () =
  let oc = open_out "results.csv" in
  fprintf oc "(rounds, steps) : average, standard deviation\n\n";
  for i = 0 to n_val-1 do
    fprintf oc "(%d,%d)   : %f, %f\n" ((i/16) + 1) ((i mod 16) + 1) (fst stats.(i)) (snd stats.(i))
  done;
  fprintf oc "\n Number of tests : %d\n" n_exp;
  close_out oc;;

read_files ();
compute_stats ();
write_stats ();;
