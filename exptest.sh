
cores="1"

case $2 in
    "")
	;;
    max)
	cores="`grep -c ^processor /proc/cpuinfo`"
	;;
    *)
	cores="$2"
	;;
esac

solver="./minisat"

case $1 in
    glucose)
	if [ -f "glucose" ];
	then
	    echo "Selected glucose solver"
	    solver="./glucose -certified"
	else
	    echo "Error: No glucose executable"
	    exit 1
	fi
	;;
    crypto*)
	if [ -f "cryptominisat4" ];
	then
	    echo "Selected cryptominisat solver"
	    solver="./cryptominisat4 -t $cores"
	else
	    echo "Error: No cryptominisat4 executable"
	    exit 1
	fi
	;;
    ""|minisat)
	if [ -f "minisat" ];
	then
	    echo "Selected minisat solver"
	    solver="./minisat"
	else
	    echo "Error: No minisat executable"
	    exit 1
	fi
	;;
    *)
	echo "Unrecognized solver"
	echo "Usage: $0 <SAT_solver> [threads]"
	exit 2
	;;
esac

for r in 2
do
	for i in `seq 16`
    do
	echo $i
	if ! ./hackMD -r $r -s $i -S "${solver}" exemples/0-digest.hex; then
	    break
	fi
    done
done
