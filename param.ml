(******************************************************************************)
(*                                                                            *)
(*                      INVERSION DE MD* VIA SAT SOLVEUR                      *)
(*                                                                            *)
(*                      Projet Logique 2016 - Partie SAT                      *)
(*   Auteur, license, etc.                                                    *)
(******************************************************************************)

open Printf

type level = Low | Normal | High | Error

let steps = ref 16 (* full MD5: 16 *)
let rounds = ref 1 (* full MD5: 4 *)
let verbose = ref High
let partialKnownInput = ref None
let partialSize = ref 0
		  
let pp = printf 

let time = Unix.gettimeofday ()

let timer = ref false

(** Prefix of each line *)
let strOfLevel = function
  | Error  -> "\027[31mError\027[0m    "
  | High   -> "\027[33m[\027[31mWARNING\027[33m]\027[0m"
  | Normal -> "(\027[32mInfo\027[0m)   "
  | Low    -> "--"
    
let log ?level:(lev=Normal) s =
  if lev == High || (lev == Normal && !verbose <> High) || !verbose == Low then
    printf "%s[%f] %s\n%!" (strOfLevel lev) (Unix.gettimeofday () -. time) s;
  if lev == Error then
    printf "%s[%f] \027[91m%s\027[0m\n%!" (strOfLevel lev) (Unix.gettimeofday () -. time) s


