rm -rf results/*;
for k in {1..1}; do
    echo $k...;
    echo ;
    for i in {1..16}; do
	export TIME="%e";
	touch results/simulation$i.log;
	./randomize.sh;
	/usr/bin/time -a -o results/simulation$i.log ./hackMD -v -r 1 -s $i exemples/random_digest.hex > /dev/null;
    done;
    
    for i in {1..12}; do
	export TIME="%e";
	j=$((16+$i));
	touch results/simulation$j.log;
	./randomize.sh;
	/usr/bin/time -a -o results/simulation$j.log ./hackMD -v -r 2 -s $i exemples/random_digest.hex > /dev/null;
    done;
done;
