
./hackMD -v -r 1 -s 1 -c exemples/honest-input.hex
./hackMD -v -r 1 -s 1 -p 224 exemples/malicious-partial-input.hex exemples/honest-input__digest.out

echo -e -n "<<"
cat exemples/honest-input.hex | xxd -r -p
echo -e -n "\n>>\n"
echo -e -n "<<"
cat exemples/honest-digest__inputFound.out | xxd -r -p
echo -e -n "\n>>"

echo -e "\n # Verification:"
./md5  exemples/honest-digest__inputFound.out
./md5  exemples/honest-input.hex
