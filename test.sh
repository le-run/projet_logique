echo -e "\e[32m     ##### [Test1 OPTIONAL] computing md5 (16 steps) ... #####\e[0m"
./hackMD -v -c exemples/0-input.hex
echo -e "\nThe implementation of reference returns..."
./md5 exemples/0-input.hex

echo -e "\n\n\e[32m     ##### [Test2 OPTIONAL] computing md5 (64 steps) ... #####\e[0m"
./hackMD -v -r 4 -s 16 -c exemples/0-input.hex
echo -e "\nThe implementation of reference returns..."
./md5 exemples/0-input.hex -full

echo -e "\n\n\e[32m     ##### [Test3] inverting md5... #####\e[0m"
./hackMD -v -r 1 -s 16 exemples/0-digest.hex

echo -e "\n\n\e[32m     ##### [Test4] inverting md5 with partial info about input... #####\e[0m"
./hackMD -v -r 1 -s 16 -p 224 exemples/malicious-partial-input.hex exemples/honest-digest.hex
echo -e "\n  # The text:"
echo -e -n "<<"
cat exemples/honest-input.hex | xxd -r -p
echo -e -n "\n>>"
echo -e "\n  # has the same MD5-digest (16 steps) as the following malicious text:"
echo -e -n "<<"
cat exemples/honest-digest__inputFound.out | xxd -r -p
echo -e -n "\n>>"
echo -e "\n # Verification:"
./md5  exemples/honest-digest__inputFound.out
./md5  exemples/honest-input.hex
